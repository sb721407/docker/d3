## SkillBox DevOps. Docker. Часть 3. Docker Builder и Dockerfile

SkillBox DevOps. Docker. Часть 3. Docker Builder и Dockerfile

--------------------------------------------------------

Сценарий работы с репозиторием:
```
cd existing_repo
git remote add origin git@gitlab.com:sb721407/docker/d3.git
git branch -M main
git push -uf origin main
```

### Задание 1. Составление Dockerfile для сборки тестового React-приложения

![Screenshot0](png/screen_0.png?raw=true "files")

### Задание 2. Сборка образа

![Screenshot1](png/screen_1.png?raw=true "docker build command")

### Задание 3. Запуск контейнера из собранного образа 

![Screenshot2](png/screen_2.png?raw=true "docker run command")

![Screenshot3](png/screen_3.png?raw=true "Browser output")



